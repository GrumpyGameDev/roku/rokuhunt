Function CreateResources(port)
    resources = {}
    resources.fontRegistry = CreateObject("roFontRegistry")
    resources.fonts = {}
    resources.fonts.default = resources.fontRegistry.GetDefaultFont()
    resources.bitmaps = {}
    resources.bitmaps.rabbit=CreateObject("roBitmap","pkg:/images/rabbit.png")
    resources.bitmaps.crosshair=CreateObject("roBitmap","pkg:/images/crosshair.png")
    resources.bitmaps.spill=CreateObject("roBitmap","pkg:/images/spill.png")
    resources.bitmaps.carrot=CreateObject("roBitmap","pkg:/images/carrot.png")
    resources.audio={}
    resources.audio.kill=CreateObject("roAudioResource","pkg:/assets/40_smith_wesson_single-mike-koenig.wav")
    resources.audio.carrot=CreateObject("roAudioResource","pkg:/assets/Apple_Bite-Simon_Craggs-1683647397.wav")
    resources.audio.miss=CreateObject("roAudioResource","pkg:/assets/bullet_whizzing_by-Mike_Koenig-2005433595.wav")
    resources.audioplayer=CreateObject("roAudioPlayer")
    resources.audioplayer.SetMessagePort(port)
    song = CreateObject("roAssociativeArray")
    song.url = "https://pdgrokuaudio.azurewebsites.net/Komiku_-_15_-_You_cant_beat_the_machine.mp3"
    resources.audioplayer.addcontent(song)
    resources.audioplayer.setloop(true)
    resources.audioplayer.play()
    return resources
End Function