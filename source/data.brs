Function CreateMazeWalker() As Object
    Return {
        values:["north","east","south","west"],
        opposites:{
            "north":"south",
            "east":"west",
            "south":"north",
            "west":"east"
        },
        deltas:{
            "north":{x:0,y:-1},
            "east":{x:1,y:0},
            "south":{x:0,y:1},
            "west":{x:-1,y:0},
        }
    }
End Function
Function CreateData()
    data= {
        columns:16,
        rows:9,
        cellwidth:80,
        cellheight:80,
        crosshair:{x:0,y:0},
        timer:0,
        walker:CreateMazeWalker(),
        mute:false
    }
    data.board = CreateBoard(data.columns,data.rows)
    ResetBoard(data)
    return data
End Function
Function ResetBoard(data)
    data.carrotsleft=0
    for column = 0 to data.columns - 1
        for row = 0 to data.rows - 1
            data.board.cells[column][row].blood=false
            data.board.cells[column][row].rabbit=false
            data.board.cells[column][row].carrot= column>0 and row>0 and column<data.columns-1 and row<data.rows-1
            if data.board.cells[column][row].carrot then
                data.carrotsleft++
            end if
        end for
    end for
End Function
Function NewLevel(data,level)
    maxlevel=(data.columns-1)*2+(data.rows-1)*2
    if level>maxlevel then
        level = maxlevel
    end if
    data.level=level
    data.bunniesleft=0
    while data.bunniesleft<data.level
        side = rnd(4)-1
        if side=0 then
            column=rnd(data.columns)-1
            row=0
        else if side=1 then
            row = rnd(data.rows)-1
            column=0
        else if side=2 then
            column=rnd(data.columns)-1
            row=data.rows-1
        else
            row = rnd(data.rows)-1
            column=data.columns-1
        end if
        if not data.board.cells[column][row].rabbit then
            data.board.cells[column][row].rabbit=true
            data.bunniesleft++
        end if
    end while
End Function
Function NextLevel(data)
    for column = 0 to data.columns - 1
        for row = 0 to data.rows - 1
            data.board.cells[column][row].rabbit=false
        end for
    end for
    NewLevel(data,data.level+1)
End Function
Function ApplyMute(context)
    if context.data.mute then
        context.resources.audioplayer.stop()
    else
        context.resources.audioplayer.play()
    end if
End Function
Function PlaySound(context,soundname)
    if not context.data.mute then
        context.resources.audio[soundname].Trigger(100)
    end if
End Function