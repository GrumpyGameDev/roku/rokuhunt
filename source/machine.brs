Function CreateMachine() As Object
    Return {
        current : "mainmenu",
        mainmenu: CreateMainMenu(),
        confirmquit: CreateConfirmQuit(),
        howtoplay: CreateHowToPlay(),
        play: CreatePlay()
    }
End Function