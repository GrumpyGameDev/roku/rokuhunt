Function CreateHowToPlay() As Object
    Return {
        fontname:"default",
        backgroundcolor:&h000000FF,
        textcolor:&hebebebff,
        titleColor:&h7575ebff,
        items:["You go through a maze!"],

        start: Function(context) As Void
        End Function,

        finish: Function (context) As Void
        End Function,

        draw: Function(context,screen) As Void
            lineHeight = context.resources.fonts[m.fontname].GetOneLineHeight() 
            screen.clear(m.backgroundcolor)
            y=0
            index=0
            screen.DrawText("How to Play", 0, y, m.titleColor, context.resources.fonts[m.fontname])
            y = y + lineHeight
            for each item in m.items
                screen.DrawText(item, 0, y, m.textcolor, context.resources.fonts[m.fontname])
                y = y + lineHeight
            end for
        End Function,

        handle: Function (context, event) As String
            if(type(event)="roUniversalControlEvent")
                button = event.GetInt()
                if(button=0)'back
                    return "mainmenu"
                end if
            end if
            Return "howtoplay"
        End Function
    }
End Function