Function Main() As Void
    port = CreateObject("roMessagePort")
    machine = CreateMachine()
    context = CreateContext(port)
    screen = CreateObject("roScreen", true)
    screen.SetMessagePort(port)
    screen.SetAlphaEnable(true)
    machine[machine.current].start(context)
    done = false
    While not done
        machine[machine.current].draw(context,screen)
        screen.SwapBuffers()
        event = port.GetMessage()
        nextState = machine[machine.current].handle(context, event)
        If (nextState="")
            done = true
        Else If (nextState<>machine.current)
            machine[machine.current].finish(context)
            machine.current = nextState
            machine[machine.current].start(context)
        End If
    End While
End Function

