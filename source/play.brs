Function CreatePlay() As Object
    Return {
        clock: CreateObject("roTimeSpan"),

        start: Function(context) As Void
            NewLevel(context.data,1)
            m.clock.Mark()
        End Function,

        finish: Function (context) As Void
        End Function,

        draw: Function(context,screen) As Void
            screen.clear(&h000000ff)
            board = context.data.board
            columns = context.data.columns
            rows = context.data.rows
            cellwidth = context.data.cellwidth
            cellheight = context.data.cellheight
            crosshair = context.data.crosshair
            moverabbit=m.clock.TotalSeconds()>=1
            if (moverabbit)
                m.clock.Mark()
                m.moverabbit(context)
            end if
            for column=0 to columns - 1
                x=column*cellwidth
                for row=0 to rows - 1
                    y=row*cellheight
                    cell = board.cells[column][row]
                    if (cell.blood)
                        screen.drawobject(x,y,context.resources.bitmaps.spill)
                    end if
                    if (cell.carrot)
                        screen.drawobject(x,y,context.resources.bitmaps.carrot)
                    end if
                    if (cell.rabbit)
                        screen.drawobject(x,y,context.resources.bitmaps.rabbit)
                    end if
                    if (column=crosshair.x and row=crosshair.y)
                        screen.drawobject(x,y,context.resources.bitmaps.crosshair)
                    end if
                end for
            end for
        End Function,

        moverabbit:Sub (context)
            x = Rnd(context.data.columns)-1
            y = Rnd(context.data.rows)-1
            cell = context.data.board.cells[x][y]
            while not cell.rabbit
                x = Rnd(context.data.columns)-1
                y = Rnd(context.data.rows)-1
                cell = context.data.board.cells[x][y]
            end while
            direction = context.data.walker.values[Rnd(context.data.walker.values.Count())-1]
            delta = context.data.walker.deltas[direction]
            nextx=x+delta.x
            nexty=y+delta.y
            if nextx>=0 and nexty>=0 and nextx<context.data.columns and nexty<context.data.rows and not context.data.board.cells[nextx][nexty].rabbit then
                cell.rabbit=false
                if cell.carrot then
                    cell.carrot=false
                    context.data.carrotsleft--
                    PlaySound(context,"carrot")
                end if
                context.data.board.cells[nextx][nexty].rabbit=true
            end if
        End Sub,

        movecrosshair:Function(context,direction)
            crosshair = context.data.crosshair
            delta = context.data.walker.deltas[direction]
            crosshair.x=(crosshair.x+context.data.columns+delta.x) MOD context.data.columns
            crosshair.y=(crosshair.y+context.data.rows+delta.y) MOD context.data.rows
        End Function,

        shoot:Function(context)
            crosshair = context.data.crosshair
            cell = context.data.board.cells[crosshair.x][crosshair.y]
            if cell.rabbit then
                cell.rabbit = false
                cell.blood = true
                context.data.bunniesleft--
                if context.data.bunniesleft<1 then
                    NextLevel(context.data)
                end if
                PlaySound(context,"kill")
            else
                PlaySound(context,"miss")
            end if
        End Function,

        handle: Function (context, event) As String
            if(type(event)="roUniversalControlEvent")
                button = event.GetInt()
                if(button=0)'back
                    return "mainmenu"
                else if(button=2)
                    m.movecrosshair(context,"north")
                else if(button=3)
                    m.movecrosshair(context,"south")
                else if(button=4)
                    m.movecrosshair(context,"west")
                else if(button=5)
                    m.movecrosshair(context,"east")
                else if(button=6)
                    m.shoot(context)
                end if
            end if
            Return "play"
        End Function
    }
End Function